# gwdc_helm
Reference repository to determine application repositories in scope and the state of their Helm charts

## Repositories
### GWCloud
| Application | State | Helm Chart | State |
| --- | --- | --- | --- |
| [gwcloud_auth](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_auth)               | ![gitlab_runner](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_auth/badges/master/pipeline.svg)          | [gwcloud_auth_helm](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_auth_helm)             | ![gitlab_runner](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_auth_helm/badges/main/pipeline.svg) |
| [gwcloud_bilby](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_bilby)             | ![gitlab_runner](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_bilby/badges/master/pipeline.svg)         | [gwcloud_bilby_helm](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_bilby_helm)           | ![gitlab_runner](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_bilby_helm/badges/main/pipeline.svg) |
| [gwcloud_db](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_db)                   | ![gitlab_runner](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_db/badges/main/pipeline.svg)                 | [gwcloud_db_helm](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_db_helm)              | ![gitlab_runner](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_db_helm/badges/main/pipeline.svg) |
| [gwcloud_db_search](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_db_search)     | ![gitlab_runner](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_db_search/badges/master/pipeline.svg)     | [gwcloud_db_search_helm](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_db_search_helm)   | ![gitlab_runner](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_db_search_helm/badges/main/pipeline.svg) |
| [gwcloud_react_host](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_react_host)   | ![gitlab_runner](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_react_host/badges/master/pipeline.svg)    | [gwcloud_react_host_helm](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_react_host_helm) | ![gitlab_runner](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_react_host_helm/badges/main/pipeline.svg) |
| [gwcloud_job_server](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_job_server)   | ![gitlab_runner](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_job_server/badges/master/pipeline.svg)    | [gwcloud_job_server_helm](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_job_server_helm) | ![gitlab_runner](https://gitlab.com/CAS-eResearch/GWDC/code/gwcloud_job_server_helm/badges/main/pipeline.svg) |
|||||

### GWLab
| Application | State | Helm Chart | State |
| --- | --- | --- | --- |
| [gwlab_cwfollowup](https://gitlab.com/CAS-eResearch/GWDC/code/gwlab_cwfollowup) | ![gitlab_runner](https://gitlab.com/CAS-eResearch/GWDC/code/gwlab_cwfollowup/badges/master/pipeline.svg) | [gwlab_cwfollowup_helm](https://gitlab.com/CAS-eResearch/GWDC/code/gwlab_cwfollowup_helm) | ![gitlab_runner](https://gitlab.com/CAS-eResearch/GWDC/code/gwlab_cwfollowup_helm/badges/main/pipeline.svg) |
| [gwlab-viterbi-cw](https://gitlab.com/CAS-eResearch/GWDC/code/gwlab-viterbi-cw) | ![gitlab_runner](https://gitlab.com/CAS-eResearch/GWDC/code/gwlab-viterbi-cw/badges/master/pipeline.svg) | [gwlab-viterbi-cw_helm](https://gitlab.com/CAS-eResearch/GWDC/code/gwlab-viterbi-cw_helm) | ![gitlab_runner](https://gitlab.com/CAS-eResearch/GWDC/code/gwlab-viterbi-cw_helm/badges/main/pipeline.svg) |
||||

### Others
| Application | State | Helm Chart | State |
| --- | --- | --- | --- |
| [meertime_dataportal](https://gitlab.com/CAS-eResearch/GWDC/code/meertime_dataportal) | ![gitlab_runner]() |[meertime_dataportal_helm](https://gitlab.com/CAS-eResearch/GWDC/code/meertime_dataportal_helm)| ![gitlab_runner](https://gitlab.com/CAS-eResearch/GWDC/code/meertime_dataportal_helm/badges/main/pipeline.svg) |
|||||


